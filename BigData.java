import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.TreeMap;
import java.util.TreeSet;

public class BigData {

	public char [] d = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	public char [] h = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	public char [] aL = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	public char [] aU = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	public char [] s = {'`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '/', '[', '{', ']', '}', '|', ';', ':', '"', ',', '<', '.', '>', '?'};
	public char [] e = {'\t', '\'', '\\', '\n'};
	
	public TreeMap<Character, Integer> dV = new TreeMap<Character, Integer>(), 
										hV = new TreeMap<Character, Integer>(), 
										aLV = new TreeMap<Character, Integer>(), 
										aUV = new TreeMap<Character, Integer>(), 
										sV = new TreeMap<Character, Integer>(), 
										eV = new TreeMap<Character, Integer>(), 
										aNSV = new TreeMap<Character, Integer>(), 
										cV = new TreeMap<Character, Integer>();
	public TreeMap<Integer, Character> dK = new TreeMap<Integer, Character>(),
										hK = new TreeMap<Integer, Character>(),
										aLK = new TreeMap<Integer, Character>(),
										aUK = new TreeMap<Integer, Character>(),
										sK = new TreeMap<Integer, Character>(),
										eK = new TreeMap<Integer, Character>(),
										aNSK = new TreeMap<Integer, Character>(),
										cK = new TreeMap<Integer, Character>();
	
	public long decodedValue = 0L;
	public StringBuilder encodedValue = new StringBuilder();
	
	public BigData() {
		initializeMaps(d, dV, dK);
		initializeMaps(h, hV, hK);
		initializeMaps(aL, aLV, aLK);
		initializeMaps(aU, aUV, aUK);
		initializeMaps(s, sV, sK);
		initializeMaps(e, eV, eK);
		initializeMaps(d, aNSV, aNSK);
		initializeMaps(aU, aNSV, aNSK, aNSV.size());
		initializeMaps(aL, aNSV, aNSK, aNSV.size());
		initializeMaps(s, aNSV, aNSK, aNSV.size());
		initializeMaps(e, aNSV, aNSK, aNSV.size());
	}
	
	public long decode(StringBuilder sb, TreeMap<Character, Integer> decodeDict) {
		long total = 0L;
		for(int i = sb.length() - 1; i > -1; i--) {
			total += decodeDict.get(sb.charAt(i)) * (Math.pow(decodeDict.size(), ((sb.length()-1)-i)) );
		}
		decodedValue = total;
		return total;
	}
	
	public StringBuilder encode(long a, TreeMap<Character, Integer> encodeDictV, TreeMap<Integer, Character> encodeDictK) {
		int count = 1;
		StringBuilder sb = new StringBuilder();
		while(a > 0) {
			int code = (int)(a %   (Math.pow(encodeDictV.size(), count)) / (Math.pow(encodeDictV.size(), count-1)));
			sb.append(encodeDictK.get(code));
			a -= a %   (Math.pow(encodeDictV.size(), count));
			count++;
		}
		sb = sb.reverse();
		encodedValue.setLength(0);
		encodedValue.append(sb);
		return sb;
	}
	
	public StringBuilder convert(StringBuilder sb, TreeMap<Character, Integer> decodeDict, TreeMap<Character, Integer> encodeDictV, TreeMap<Integer, Character> encodeDictK) {
		return encode(decode(sb, decodeDict), encodeDictV, encodeDictK);
	}
	
	public void initializeMaps(char [] d, TreeMap<Character, Integer> dV, TreeMap<Integer, Character> dK) {
		for(int i = 0; i < d.length; i++) {
			dV.put(d[i], i);
			dK.put(i, d[i]);
		}
	}
	
	public void initializeMaps(char [] d, TreeMap<Character, Integer> dV, TreeMap<Integer, Character> dK, int start) {
		for(int i = 0; i < d.length; i++) {
			dV.put(d[i], i+start);
			dK.put(i+start, d[i]);
		}
	}
	
	public void initializeCustomMap(char [] c) {
		initializeMaps(c, cV, cK, cV.size());
	}
	
	public char [] getUniqueChars(File f) {
		TreeSet<Character> tree = new TreeSet<Character>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			int c = br.read();
			while(c > -1) {
				tree.add((char)c);
				c = br.read();
			}
			br.close();
		} catch (Exception e) {}
		char [] c = new char[tree.size()];
		for(int i = 0; i < c.length; i++) {
			c[i] = tree.first();
			tree.remove(tree.first());
		}
		return c;
	}
	
	public void compressFile(File f) {
		char [] cc = getUniqueChars(f);
		initializeCustomMap(cc);
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File("test3.txt")));
			int c = br.read();
			StringBuilder sb = new StringBuilder();
			while(c > -1) {
				sb.append((char)c);
				if(sb.length() == 8) {
					bw.write(convert(sb, cV, aNSV, aNSK).toString());
					sb.setLength(0);
				}
				c = br.read();
			}
			br.close();
			bw.flush();
			bw.close();
		} catch (Exception e) {}
		
	}
	
}
