/*TEMPORARY DISCLAIMER: ALL OF THIS IS MORE OR LESS JUST REPACKAGED CODE FROM JAVA'S INTERNAL LIBRARIES FOR CONVENIENCE. I'M NOT TRYING TO TAKE CREDIT FOR ANYTHING HERE.*/
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import java.io.File;
import javax.swing.JFileChooser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class Methods {
    public FileReader fr; public BufferedReader br;
	public FileWriter fw; public BufferedWriter bw;

    //Toolkit, DataFlavor, HeadlessException, UnsupportedFlavorException, IOException
    //MODIFIED FROM STACKOVERFLOW: https://stackoverflow.com/a/7106086
    //Thank you stackoverflow user "Dragon8"!
	public String ClipGet() {
		try {return (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);}
		catch (HeadlessException e)             {System.out.println("HEADLESS_EXCEPTION");e.printStackTrace();}
		catch (UnsupportedFlavorException e)    {System.out.println("UNSUPPORTED_FLAVOR_EXCEPTION");e.printStackTrace();}
		catch (IOException e)                   {System.out.println("IO_EXCEPTION");e.printStackTrace();}
		return null;
	}

    //File, JFileChooser
    public File chooseFile() {
		JFileChooser chooser = new JFileChooser("");
		chooser.showOpenDialog(null);	
		return new File(chooser.getSelectedFile().getPath());
	}

    //BufferedReader, BufferedWriter, File, FileNotFoundException, FileReader, FileWriter, IOException
    public BufferedReader openReader(File f) {
		try {fr = new FileReader(f);br = new BufferedReader(fr);return br;}
		catch (FileNotFoundException e) {e.printStackTrace();}
		return null;
	}
	public void closeReader() {
		try {fr.close(); br.close();}
		catch (IOException e) {e.printStackTrace();}
	}
	public BufferedWriter openWriter(File f) {
		try {fw = new FileWriter(f);bw = new BufferedWriter(fw);return bw;}
		catch (IOException e) {e.printStackTrace();}
		return null;
	}
	public void closeWriter() {
		try {bw.flush();fw.flush();bw.close();fw.close();}
		catch (IOException e) {e.printStackTrace();}
	}

    //BufferedReader, File, FileReader, IOException
    public void printFileToConsole(File f, int sleep) throws  InterruptedException, IOException{
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();
		while(line != null) {
			System.out.println(line);
			Thread.sleep(sleep);
			line = br.readLine();
		}
		br.close();fr.close();
	}

    //COPIED FROM STACKOVERFLOW: https://stackoverflow.com/a/240630
    //Thank you stackoverflow user "Chris Marasti-Georg"!
   	public String removeHTMLTags(String s){
	    return s.replaceAll("\\<[^>]*>","");
	}

    //MODIFIED FROM STACKOVERFLOW: https://stackoverflow.com/a/13632114
    //Thank you stackoverflow user "ccleve"!
    public String readHTML(String site) {
		String s = null;
		try {
			URL url = new URL(site);
			try {
				Scanner sc = new Scanner(url.openStream(), "UTF-8");
			    s = sc.useDelimiter("\\A").next();
                sc.close();
			}
			catch (IOException e) {e.printStackTrace();}
		}
		catch (MalformedURLException e) {e.printStackTrace();}
	    return s;
	}

    public String [] csvLineToArray(String s, String delim, int numOfColInCSV, boolean removeFirstAndLastCh) {
		try {
			if(removeFirstAndLastCh) { s = s.substring(1,s.length()-1);}
			String [] t = new String[numOfColInCSV];
			for(int i = 0; i < t.length-1; i++) {
				int p = s.indexOf(delim);
				t[i] = s.substring(0, p);
				s = s.substring(p+delim.length());
			}
			t[t.length-1] = s;
			return t;
		}
		catch (Exception e) {
			return null;
		}
	}
}